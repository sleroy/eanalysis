`EAnalysis is not compatible with macOS 10.15 Catalina. Use iAnalyse 5.`

“The term sound-based music typically designates the art form in which the sound, that is, not the musical note, is its basic unit.” Leigh Landy, Understanding the Art of Sound Organization.

The development of EAnalysis is part of the research project entitled ‘New multimedia tools for electroacoustic music analysis’ at the [MTI Research Centre]( https://www.dmu.ac.uk/home.aspx) of De Montfort University (Leicester, UK). The initial project (2010-2013) was funded by the [Arts and Humanities Research Council](https://ahrc.ukri.org) (AHRC).

This piece of software aims at experimenting new types of graphic representations and new analytical methods with an intuitive interface and adapted tools for analysis purposes.

![](https://forum.ircam.fr/media/uploads/software/EAnalysis/oiseau.jpg)

## Features ##

- Visualise sonogram (linear, logarithmic, differential, similarity matrix) and waveform,
- Import several audio and/or movie files to analyse multitrack works or compare different works,
- Work with video to analyse video music, soundtrack, performance, sound installation, etc…,
- Create beautiful representations with graphic events on different layers,
- Analyse with analytical events and sound/musical parameters,
- Create your own analytical lists of parameters and share them,
- Annotate during playback with time text,
- Use graphic tablet or interactive whiteboard to draw representation,
- Compute audio descriptors with [libxtract Vamp plugins](https://www.vamp-plugins.org/download.html),
- Compute filters with [SuperVP](https://forum.ircam.fr/projects/detail/supervp-for-max/) or [AudioSculpt](https://forum.ircam.fr/projects/detail/audiosculpt/) from graphic annotations (rectangle, polygons, drawing),
- Use several types of views in the same interface,
- Create charts and maps from sound extracts: paradigmatic chart, generative tree, soundscape map, etc,
- Create synchronised slideshow,
- Create layers of sonograms from several tracks to analyse space motions, difference between versions of same work, or different works,
- Create (self-)similarity matrix, [BStD graphs](https://hal.archives-ouvertes.fr/hal-01265269/), cloud points or correlation graph from audio descriptors or imported data,
- Save configurations (snapshots),
- Import data from other software like Sonic Visualiser (annotation layer), [Audiosculpt](https://forum.ircam.fr/projects/detail/audiosculpt/) (markers and audio descriptors) or Acousmographe,
- Import Pro Tools information sessions and create graphic representation from sound clips,
- Export to images, movies, and text files (txt, csv, xml, json),
- Export without media to share analysis without copyright restrictions.

![](https://forum.ircam.fr/media/uploads/software/EAnalysis/oiseauzen-836x500.png)

## Minimum system requirement ##

- Macintosh Multicore Intel Processor, MacOS 10.7 to 10.14, 4GB RAM
(No Windows version will be developed)

> - **[Official site](http://logiciels.pierrecouprie.fr/?page_id=402)**’s
> -**Research and development:** Dr Pierre Couprie
> - **Coordination:** Prof Simon Emmerson & Prof Leigh Landy
> - [Movie tutorials](https://www.youtube.com/watch?v=9wVY8sCTRyw&feature=youtu.be&list=PLAF1ABD80B8ED2734)